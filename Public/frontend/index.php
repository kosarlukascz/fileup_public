<?php
include 'inc/inc.php';
?>
<?php
$uzivatele = dibi::fetchSingle('SELECT COUNT(*) FROM uzivatele');
$soubory = dibi::fetchSingle('SELECT COUNT(*) FROM soubory');
$celkovavelikost = dibi::fetchSingle('select sum(velikost) from soubory');
?>
<?php
function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', ' KB', ' MB', ' GB', ' TB');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}
?>

<? include'inc/head.php'; ?>
<body>
  <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
  <header id="home">
    <? include'inc/slider.php'; ?>
    <div class="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">
            <h1><i style="color: white;" class="fa fa-cloud-upload"></i><span style="color: white;"> FileUP!</span> </h1>
          </a>                    
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">                 
            <li class="scroll active"><a href="#home">Home</a></li>
            <li class="scroll"><a href="#services">Výhody</a></li> 
            <li class="scroll"><a href="#about-us">O Nás</a></li>
            <li class="scroll"><a href="#pricing-table">Začít</a></li>
            <li class="scroll"><a href="#contact">Kontakt</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>
  <section id="services">
    <div class="container">
      <div class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="row">
          <div class="text-center col-sm-8 col-sm-offset-2">
            <h4><span>FileUP.cz</span> je nový portál pro Vaše soubory! Není nic jednoduššího, než nahrát Vaše vzpomínky do bezpečného místa, a mít k nim přístup odkudkoli na světě!</h4>
            <p></p>
          </div>
        </div> 
      </div>
      <div class="text-center our-services">
        <div class="row">
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="service-icon">
              <i class="fa fa-connectdevelop"></i>
            </div>
            <div class="service-info">
              <h3>Přístup všude!</h3>
              <p>Náš web je Vám k dispozici celý rok, každý den a každou minutu z jakéhokoliv místa na světě. </p>
            </div>
          </div>
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
            <div class="service-icon">
              <i class="fa fa-user-plus"></i>
            </div>
            <div class="service-info">
              <h3>Registrace přináší výhody!</h3>
              <p>Registrovaní uživatelé mají u nás přednost a můžou využívat více skvělých výhod!</p>
            </div>
          </div>
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="550ms">
            <div class="service-icon">
              <i class="fa fa-cloud"></i>
            </div>
            <div class="service-info">
              <h3>Svoje soubory neztratíte!</h3>
              <p>Vaše soubory jsou u nás v bezpečí a není nic, co by je mohlo ohrozit!</p>
            </div>
          </div>
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="650ms">
            <div class="service-icon">
              <i class="fa fa-diamond"></i>
            </div>
            <div class="service-info">
              <h3>Uživatel na prvním místě!</h3>
              <p>My jsme tu pro Vás, chceme, aby jste se cítili pohodlně, ať už při prohlížení, nebo nahrávání Vašich souborů.</p>
            </div>
          </div>
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="750ms">
            <div class="service-icon">
              <i class="fa  fa-server"></i>
            </div>
            <div class="service-info">
              <h3>Profesionální server!</h3>
              <p>Naše aplikace je umístěna v profesionálním datovém centru, které zaručí vysokou kvalitu našeho serveru!</p>
            </div>
          </div>
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="850ms">
            <div class="service-icon">
              <i class="fa fa-money"></i>
            </div>
            <div class="service-info">
              <h3>Za nás neutratíte!</h3>
              <p>Spousta místa zdarma! Co víc si může uživatel FileUP přát?</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="about-us" class="parallax">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2>O Nás</h2>
            <p><b>FileUP.cz</b> je nový projekt, který Vám umožní nahrávání, sdílení a ukládání Vašich snímků na bezpečné místo. <b>FileUP.cz</b> Vám nabídne každým dnem něco nového, protože se neusále vylepšuje a jeho možnosti se rozšiřují! </p>
            <p>Myšlenka našeho serveru vznikla v roce 2014, kdy jsme neznali na českém internetu službu, která by nabízela to, co nabízíme my. <b>Bezplatné</b> uložiště obrazků, které není ničím omezeno. My si stojíme za tím, aby byli naši uživatelé s naší službou spokojeni a vraceli se zpět k nám!</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="our-skills wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
              <p class="lead">Spokojenost uživatelů</p>
              <div class="progress">
                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  aria-valuetransitiongoal="85">85%</div>
              </div>
            </div>
            <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="400ms">
              <p class="lead">Vzhled webu</p>
              <div class="progress">
                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  aria-valuetransitiongoal="50">50%</div>
              </div>
            </div>
            <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="500ms">
              <p class="lead">Stádium webu</p>
              <div class="progress">
                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  aria-valuetransitiongoal="35">35%</div>
              </div>
            </div>
            <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
              <p class="lead">Naše nadšení</p>
              <div class="progress">
                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"  aria-valuetransitiongoal="100">100%</div>
              </div>
            </div>
          </div>
        </div>
      </div>
       <br /><br />
      <div class="row count parallax"  id="features">
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
          <i class="fa fa-user"></i>
          <h3 class="timer"><?php
              echo $uzivatele;
              ?></h3>
          <p>Uživatelů</p>
        </div>
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="500ms">
          <i class="fa fa-desktop"></i>
          <h3 class="timer"><?php
              echo $soubory;
              ?></h3>
          <p>Nahraných obrázků</p>
        </div>
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="700ms">
          <i class="fa fa-hdd-o"></i>
          <h3 class=""><?= formatBytes($celkovavelikost) ?></h3>
          <p>Prostoru zaplněno!</p>
        </div>
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="900ms">
          <i class="fa fa-comment-o"></i>
          <h3>24/7</h3>
          <p>Rychlá podpora</p>
        </div>
      </div>
    </div>
  <p><h3 class="wow fadeIn" align="center">Tak už neváhejte a začněte využívat <span>FileUP.CZ</span> bez omezení. Od užívání naší služby Vás už dělí jen jeden krok!</h3><br />
  </section>
  <script>
      $(function() {

          $(window).on('load', function() {
                $('#opener').on('click', function() {
                    if($('#vysun').css('display') == 'none') {
                        $('#vysun').show();
                    } else {
                        $('#vysun').hide();
                    }
                }) ;
          });

      });
  </script>
  <section id="pricing-table" class="parallax">
      <div class="container">
          <div class="row">
              <div class="pricing" >
                  <div class="col-md-6 col-sm-12 col-xs-12"> <!-- S registací-->
                      <div class="pricing-table wow fadeInDown">
                          <div class="pricing-header">
                              <p class="pricing-title">S registrací</p>
                              <p class="pricing-rate"></p>
                              <a href="http://user.fileup.cz/" class="btn btn-custom">Přejít do služby!</a>
                          </div>

                          <div class="pricing-list">
                              <ul>
                                  <li><i class="fa fa-check"></i>Neomezený prostor</li>
                                  <li><i class="fa fa-check"></i>Vygenerování odkazu</li>
                                  <li><i class="fa fa-check"></i><span>Uchováno</span> navždy</li>
                                  <li><i class="fa fa-check"></i>Vlastní galerie</li>
                                  <li><i class="fa fa-check"></i>Možnost nahrávat více souborů najednou</li>
                              </ul>
                          </div>
                      </div>
                  </div> <!-- //S registací-->

                  <div class="col-md-6 col-sm-12 col-xs-12"> <!-- Bez registace-->
                      <div class="pricing-table wow fadeInDown">
                          <div class="pricing-header">
                              <p class="pricing-title">Bez registrace</p>
                              <p class="pricing-rate"></p>
                              <a id="opener" class="btn btn-custom">Nahrát soubor!</a>
                          </div>

                          <div class="pricing-list">
                              <ul>
                                  <li><i class="fa fa-check"></i>Možnost nahrávat soubory</li>
                                  <li><i class="fa fa-check"></i>Vygenerování odkazu</li>
                                  <li><i class="fa fa-times"></i><span>Uchováno</span> navždy</li>
                                  <li><i class="fa fa-times"></i>Vlastní galerie</li>
                                  <li><i class="fa fa-times"></i>Možnost nahrávat více souborů najednou</li>
                              </ul>
                          </div>
                      </div>
                  </div> <!-- //Bez registace-->
              </div>
          </div> <!-- //row-->
          <div class="row">
            <!-- Vysunovací box - upload bez registrace--->
              <div id="vysun" style="display: none;" class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="row">
                      <br />
                    <div class="vysunovaci col-lg-12" id="vysunovaci">
                        <div class="col-lg-6 wow fadeInRight">
                            <p >Jako neregistrovaný uživatel si u nás můžete nahrát najednou pouze jeden soubor. Váš soubor bude smazán vždy na začátku měsíce. Nahrávat můžete soubory s příponou <span>JPG, GIF a PNG.</span> V případě, že Vám tato služba nevyhovuje, zvažte prosím <a href="administrace">registraci</a> Vašeho účtu. <br/> <span>
                                Je to zdarma. </span></p>
                        </div>
                        <?php
                        // konfigurace
                        $uploadDir = './docasne'; // adresar, kam se maji nahrat obrazky (bez lomitka na konci)
                        $allowedExt = array('jpg', 'jpeg', 'png', 'gif'); // pole s povolenymi priponami
                        $results = array();

                        // zpracovani uploadu
                        if(isset($_FILES['obrazky']) && is_array($_FILES['obrazky']['name'])) {

                            $counter = 0;
                            $allowedExt = array_flip($allowedExt);
                            foreach($_FILES['obrazky']['name'] as $klic => $nazev) {

                                $fileName = basename($nazev);
                                $tmpName = $_FILES['obrazky']['tmp_name'][$klic];
                                $size = $_FILES['obrazky']['size'][$klic];

                                // kontrola souboru
                                if(
                                    !is_uploaded_file($tmpName)
                                    || !isset($allowedExt[strtolower(pathinfo($fileName, PATHINFO_EXTENSION))])
                                ) {
                                    // neplatny soubor nebo pripona
                                    continue;
                                }

                                $pap = $uploadDir.DIRECTORY_SEPARATOR.time()."-".$fileName;
                                $pap2 = $uploadDirThumb.DIRECTORY_SEPARATOR.time()."-".$fileName;
                                $pripona = pathinfo($fileName);

                                // presun souboru
                                if(move_uploaded_file($tmpName, $pap)) {
                                    ++$counter;


                                    ($uploadDir.DIRECTORY_SEPARATOR.time()."-".$fileName);



                                    $results[] = "<a target='_blank' href='http://fileup.cz/docasne/".time()."-".$fileName."'><button class='btn bg-maroon btn-flat btn-block margin'><b><p>".$counter." z ".sizeof($_FILES['obrazky']['name'])." souborů.</b></button></a>";
                                                                    }
                            }
                        }
                        ?>
                        <div class="col-lg-6 wow fadeInLeft">
                            <form method="post" id="nahraj" enctype="multipart/form-data">
                                <input type="file" class="btn btn-info btn-lg btn-block" name="obrazky[]" /> <br />
                                <span id="submitContent"><input type="submit" class="btn btn-danger btn-lg btn-block" id="submitButton" value="Nahrát" /></span>
                            </form>
                        </div>
                        </div>
                    </div> <!-- row-->
                        <div class="row">
                            <div class="col-lg-12">
                                <a name="tady"></a>
                                <? foreach($results as $result) {echo $result; }?>
                            </div>
                            </div>
                  </div>
              </div>
              <!-- //Vysunovací box - upload bez registrace--->
          </div>
  </section>
  <section id="contact">
      <?
      mb_internal_encoding("UTF-8");

      $hlaska = false;
      if (isset($_GET['uspech']))
          $hlaska = '<p class="btn-warning">Email byl úspěšně odeslán, brzy vám odpovíme.</p>';
      if ($_POST) // V poli _POST něco sje, odeslal se formulář
      {
          if (isset($_POST['jmeno']) &&
              isset($_POST['email']) &&
              isset($_POST['zprava'])) {
              $hlavicka = 'From:' . $_POST['email'];
              $hlavicka .= "\nMIME-Version: 1.0\n";
              $hlavicka .= "Content-Type: text/html; charset=\"utf-8\"\n";
              $adresa = 'admin@fileup.cz';
              $predmet = 'Nová zpráva z FileUP od ' . $_POST['jmeno'];
              $uspech = mb_send_mail($adresa, $predmet, $_POST['zprava'], $hlavicka);
              if ($uspech)
              {
                  $hlaska = '<p class="btn-warning">Email byl úspěšně odeslán, brzy vám odpovíme.</p>';
                  echo'<meta http-equiv="refresh" content="0; url=index.php?uspech=ano#contact" />';
                  exit;
              }
              else
                  $hlaska = '<p class="btn-warning">Email se nepodařilo odeslat. Zkontrolujte adresu.</p>';
                   echo'<meta http-equiv="refresh" content="0; url=index.php#contact" />';
          }
          else
              $hlaska = '<p class="btn-warning">Formulář není správně vyplněný!</p>';
          echo'<meta http-equiv="refresh" content="0; url=index.php#contact" />';
      }

      ?>
    <div id="contact-us" class="parallax">
      <div class="container">
        <div class="row">
          <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2>Kontaktuj nás!</h2>
            <p>V případě nějakých nejasností ohledně naší služby <b>FileUP.cz</b> nás neváhejte kontaktovat, rádi Vám odpovíme na Vaše otázky!</p>
          </div>
        </div>
        <?php 
            if ($hlaska)
                echo '<p>' . $hlaska . '</p>';
        
            $jmeno = (isset($_POST['jmeno'])) ? $_POST['jmeno'] : '';
            $email = (isset($_POST['email'])) ? $_POST['email'] : '';
            $zprava = (isset($_POST['zprava'])) ? $_POST['zprava'] : '';
        ?>
        <div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="row">
            <div class="col-sm-6">
              <form id="main-contact-form" method="POST">
                <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="text"  name="jmeno" value="<?= htmlspecialchars($jmeno) ?>" class="form-control" placeholder="Sem napište vaše jméno." required="required" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="email" name="email" class="form-control" placeholder="Sem napište Vaši E-Mailovou adresu" required="required" value="<?= htmlspecialchars($email) ?>"/>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input name="rok" class="form-control" placeholder="Sem napište, jaký je rok." required="required">
                </div>
                <div class="form-group">
                  <textarea rows="4"  class="form-control" placeholder="Zpráva..." name="zprava"><?= htmlspecialchars($zprava) ?></textarea>
                </div>                        
                <div class="form-group">
                  <input type="submit" value="Odeslat" class="btn-submit" />
                </div>
              </form>   
            </div>
            <div class="col-sm-6">
              <div class="contact-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                <p>V případě potřeby nás kontaktovat využijte prosím uvedený formulář, na Vaše dotazy se budeme snažit co nejdříve odpovědět.! Děkujeme za Vaše ohlasy!</p>
              </div>                            
            </div>
          </div>
        </div>
      </div>
    </div>        
  </section>
<? include'inc/footer.php'; ?>