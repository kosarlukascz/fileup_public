<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="FileUP.cz | Uložiště obrázků všeho druhu! Sdílej svoje vzpomínky a šiř je mezi přátelé, a kdekoliv na světě! FileUP! Vás neomezuje.">
<meta name="author" content="FileUP!">
<meta name="copyright" content="Všechna práva vyhrazena." />
<meta name="robots" content="index,follow" />
<meta name="DC.title" content="Uložiště všeho druhu - zdarma pro Vás!" />
<meta name="keywords" content="uložiště,cloud,obrazky,upload,neomezeno,administrace,rozhrani,o nas,cesky,web,soubory,design,neomezeny,pristup,možnost,moderní,začít,prostor,nadšenost,ohlasy,,obrazku,nahravani,zdarma,file,up,fileup,aplikace,windows,phone,android,apple,ios,rychle,bezplatne,uloziste,soubory," />
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74250320-1', 'auto');
    ga('send', 'pageview');

</script>