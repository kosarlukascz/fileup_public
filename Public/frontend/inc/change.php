<? include_once 'dibi/loader.php';
dibi::connect(array(
    'driver'   => 'mysql',
    'host'     => 'wm95.wedos.net',
    'username' => 'a114311_fileup',
    'database' => 'd114311_fileup',
    'password' => '38Om,6mY',
));
?>
<? $aktualniVerze = '0.5.0'; ?>
<?
{
    $arr1 = [
        'verze' => $aktualniVerze
    ];
    dibi::update('verze', $arr1)->where(' id = %i ', 1)->execute();
}?>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Changelog</h3>
            </div>
            <div class="box-body">
<!-- Changelog -->
                <h2>0.5.0</h2>
                <p>
                <h3>Novinky</h3>
                <ul>
                    <li>Vytvořena galerie souborů.</li>
                    <li>Změněno šifrování hesel pro větší bezpečnost.</li>
                    <li>Vytvořen a předělán upload souborů na web.</li>
                    <li>Vytvořena galerie Vámi nahraných souborů.</li>
                    <li>Vytvořeny veřejné statistiky.</li>
                    <li>Odstaněn bug s nemožností odeslat zprávu z webu.</li>
                    <li>V sekci upload má uživatel přehled o jeho zaplnění účtu. Vidí obsazenost svého uložiště v procentech a číslech.</li>
                    <li>Vytvořeno tlačítko pro správů uživatelského účtu a odhlášení pro uživatele, které používají mobilní telefon nebo tablet.</li>
                </ul>
                <h3>Známé bugy</h3>
                <ul>
                    <li>Nastane chyba při nahrávání velkého souboru.</li>
                </ul>
                </p>
<h2>0.3.0</h2>
<p>
<h3>Novinky</h3>
<ul>
<li>Vytvořeno kompletně nové uživatelské rozhraní webu. </li>
<li>Vytvořeny notifikace a jejich správa. </li>
<li>Vytvořena registrace a přihlášení.</li>
<li>Všechny uživatelské údaje jsou nově zapisovány do databáze šifrovaně.</li>
</ul>
<h3>Známé bugy</h3>
<ul>
<li>Při použití kontaktního formuláře se stránka "zasekne", mail se odešle / neodešle korektně.</li>
</ul>
</p>
<h2>0.2.1</h2>
<p>
<h3>Změny</h3>
<ul>
<li>Opraven kontaktní formulář - chyba byla zjištěna náhodně. </li>
<li>Upraveno směrování a hlášení kontaktního formuláře.</li>
<li>Při dočasném uploadu opravena chyba - možnost nahrávat více souborů.</li>
</ul>
<h3>Známé bugy</h3>
<ul>
<li>Při uplodávní více souborů může FileUP přestat odpovídat.</li>
<li>Soubory s velmi specifickým pojmenováním nevytvoří vždy korektní odkaz (skrze galerii jsou funkční).</li>
<li>Při použití kontaktního formuláře se stránka "zasekne", mail se odešle / neodešle korektně.</li>
</ul>
</p>
 <h2>0.2</h2>
<p>
<h3>Změny</h3>
<ul>
    <li>Spuštěno odpočítávní do nové verze, která již bude první finální - 20. Května 2015. </li>
    <li>Proběhly velké změny v kódu a celkové úpravy funkčnosti úvodního webu.</li>
    <li>Úvodní web je přešel na novější, rychlejší a lepší datbázový systém.</li>
    <li>Základní optimalizace SEO.</li>
    <li>Změny v barvách projektu.</li>
    <li>Přepracován upload soborů, přidány statistiky.</li>
    <li>Vylepšený systém zobrazování nahraných soborů.</li>
</ul>
<h3>Známé bugy</h3>
<ul>
<li>Při uplodávní více souborů může FileUP přestat odpovídat.</li>
<li>Soubory s velmi specifickým pojmenováním nevytvoří vždy korektní odkaz (skrze galerii jsou funkční).</li>
</ul>
 </p>
<h2>0.1 </h2>
<p>
<h3>Změny</h3>
<ul>
<li>Přidán changelog.</li>
<li>Resetována databáze z důvodů změn některých nutntností.</li>
<li>Přechod na samostantný hosting.</li>
<li>Změna databáze - vytvořena odělená databáze pro soubory a uživatelské účty.</li>
<li>Změněna registrovací nabídka - vyžaduje email (zapomenuté heslo, newslatter).</li>
<li>Změna databáze - vytvořena odělená databáze pro soubory a uživatelské účty.</li>
<li>Vytvořen základní rys mobilní aplikace.</li>
</ul>
<h3>Známé bugy</h3>
<ul>
<li>Při uplodávní více souborů může FileUP přestat odpovídat.</li>
<li>Soubory s velmi specifickým pojmenováním nevytvoří vždy korektní odkaz (skrze galerii jsou funkční).</li>
</ul>
</p>
<!-- /Changelog -->
</div>
        </div>
        <div class="box-footer">
            <p>Seznam změn které proběhli ve vývoji.</p>
        </div>
    </div>
</div>
