<footer id="footer"><!--- patička -->
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="container text-center">
            <div class="footer-logo">
                <a href="index.php"><h1><i style="color: white;" class="fa fa-cloud-upload"></i><span style="color: white;"> FileUP!</span> </h1></a>
            </div>
            <div class="social-icons">
                <ul>
                    <li><a class="twitter" href="https://twitter.com/search?f=tweets&vertical=default&q=projektblbost&src=typd"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="facebook" href="https://www.facebook.com/lukas.kosar"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p>&copy; 2015 FileUP.CZ</p>
                </div>
                <div class="col-sm-6">
                    <p class="pull-right"><a href="http://www.fileup.cz/">FileUP.cz</a></p>
                    <?
                    ob_start();
                    include 'change.php';
                    ob_end_clean();
                    ?>
                    <p class="pull-left"><a href="change.php"><? echo $aktualniVerze; ?></a></p>
                </div>
            </div>
        </div>
    </div>
</footer><!--- /patička -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="js/jquery.inview.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/mousescroll.js"></script>
<script type="text/javascript" src="js/smoothscroll.js"></script>
<script type="text/javascript" src="js/jquery.countTo.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script>
    $('#submitButton').on('click',function() {
        $('#nahraj').submit();
        $('#submitContent').html('<i class="fa fa-spinner fa-spin"></i> Nahrávání');
    });
</script>


</body>
</html>