<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.php" class="site_title"><i class="fa fa-cloud-upload"></i> <span>FileUP!</span></a>
                </div>
                <div class="clearfix"></div>

                <?php
                $items = dibi::query("select * from uzivatele WHERE mail ='{$_SESSION['mail']}'");
                ?>
                <!-- Uživatel - info  -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="images/img.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Vítejte, </span>
                        <?php foreach($items as $key => $item) { ?>
                        <h2><?=$item['cele_jmeno']?></h2> <? } ?>
                    </div>
                </div>
                <!-- /Uživatel - o -->

                <br />
                <? include 'menu.php'; ?>

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" href="settings.php" data-placement="top" title="Nastavení">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" href="index.php?odhlasit" data-placement="top" title="Odhlásit">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>