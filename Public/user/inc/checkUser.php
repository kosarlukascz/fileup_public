<?php

if (!isset($_SESSION['uzivatel_id']))
{
    redir('login.php');
}

if (isset($_GET['odhlasit']))
{
    $_SESSION = array();
    redir('login.php');
}