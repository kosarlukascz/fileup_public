<!-- menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div>
        <ul class="nav side-menu">
            <li><a href="index.php"><i class="fa fa-home"></i> Přehled </a></li>
            <li><a href="upload.php"><i class="fa fa-cloud-upload"></i> Nahrávání </a></li>
            <li><a href="gallery.php"><i class="fa fa-photo"></i> Galerie </a></li>
            <li><a href="rules.php"><i class="fa fa-pencil"></i> Pravidla <span class="label label-success pull-right">V přípravě</span> </a></li>
            <li><a href="contact.php"><i class="fa fa-send-o"></i> Kontakt <span class="label label-success pull-right">V přípravě</span> </a></li>
            <li class="hidden-lg hidden-md"><a><i class="fa fa-cog"></i> Možnosti <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none;">
                    <li><i class="fa fa-cog"></i><a href="settings.php">Účet</a></li>
                    <li><i class="fa fa-power-off"></i><a href="index.php?odhlasit">Odhlásit</a></li>
                </ul>
            </li>
        </ul>
    </div>

</div>
<!-- / menu -->