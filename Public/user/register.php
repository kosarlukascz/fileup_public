<?php include'inc/inc_neprihlasen.php';?>
<?php

if ($_POST && isset($_POST['rok']) && isset($_POST['heslo']) && isset($_POST['heslo_znovu']))
{
    if ($_POST['rok'] != date('Y'))
        $zprava = '<p class="bg-danger">Chybně vyplněný antispam.</p>';
    else if ($_POST['heslo'] != $_POST['heslo_znovu'])
        $zprava = '<p class="bg-danger">Hesla nesouhlasí</p>';
    else
    {
        $existuje = Db::query('SELECT mail FROM uzivatele WHERE mail = ? LIMIT 1', $_POST['mail']);
        if ($existuje)
            $zprava = '<p class="bg-danger">Uživatel s tímto e-mailem je již v databázi obsažen.</p>';
        else
        {
            Db::query('        INSERT INTO uzivatele (mail, heslo, cele_jmeno )
                                VALUES (?, SHA1(?),?)
                        ', $_POST['mail'], $_POST['heslo'] . "t&#ssdf54gh",$_POST['cele_jmeno']);
            $_SESSION['uzivatel_id'] = Db::getLastId();
            $_SESSION['mail'] = $_POST['mail'];
            $_SESSION['uzivatel_admin'] = 0;
            $_SESSION['cele_jmeno'] = $_POST['cele_jmeno'];
            header('Location: index.php');
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FileUP.cz | Registrace </title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background:#F7F7F7;">
<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>
    <div id="wrapper">
        <div id="login" class="animate form">
            <section class="login_content">
                <form method="post">
                    <h1>Zaregistrujte se</h1>
                    <?php
                    if (isset($zprava))
                        echo('<p>' . $zprava . '</p>');
                    ?>
                    <div>
                        <input type="text" name="cele_jmeno" class="form-control" min="5" placeholder="Jméno a příjmení" required="required" />
                    </div>
                    <div>
                        <input type="email" name="mail" class="form-control" placeholder="Váš e-mail" required="required" />
                    </div>
                    <div>
                        <input type="password" name="heslo" class="form-control" placeholder="Heslo" required="required" min="8" />
                    </div>
                    <div>
                        <input type="password" name="heslo_znovu" class="form-control" placeholder="Heslo znovu" required="required" min="8" />
                    </div>
                    <div>
                        <input type="text" name="rok" class="form-control" placeholder="Jaký je právě rok? - Antispamová otázka" required="required" min="8" />
                    </div>
                    <div class="center-margin">
                        <input type="submit" class="btn btn-default submit" value="Registrujte se" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">
                        <div>
                            <h1><i class="fa fa-cloud-upload" style="font-size: 26px;"></i> FileUP.cz</h1>

                            <p>&copy; 2016 </p>
                        </div>
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
    </div>
</div>

</body>

</html>
