<?php include'inc/inc.php';?>
<?php
$soubory = dibi::query('select * from soubory where user_id = ' . $_SESSION['uzivatel_id']);
?>
<?
if(fg('akce', 'smazat')) {
    $link = dibi::fetchSingle("select img_link from soubory where id = %i;", get('id'));
    $nahled = dibi::fetchSingle("select img_link_thumb from soubory where id = %i;", get('id'));
    dibi::delete('soubory')->where(' id = %i ', get('id'))->execute();

    redir('gallery.php');}
    ?>
    <!DOCTYPE html>
    <html lang="en">
<head>
    <title>FileUP | Galerie </title>
    <? include'inc/head.php'; ?>
    <? include'inc/right.php'; ?>
    <? include'inc/header.php'; ?>
    <!--  content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3> Media Gallery <small> gallery design</small> </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Galerie souborů</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <!-- fotka - PHP výpis -->
                                <?php
                                foreach($soubory as $key => $item) {
                                    echo '<div class="col-md-55">
                                            <div class="thumbnail">
                                        <div class="image view view-first">';
                                    echo'<img style="width: 100%; display: block;" src="'.$item['img_link_thumb'].'" alt="image" />';
                                    echo '<div class="mask">
                                                <div class="tools tools-bottom">
                                                    <a href="#"><i class="fa fa-link"></i></a>
                                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                                    <a href="#"><i class="fa fa-times"></i></a>
                                                    <a href="'.$item['img_link'].'" title="Zobrazit v plné velikosti" data-gallery><i class="fa fa-photo"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>';
                                }
                                ?>
                                <!-- /fotka - PHP výpis -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" >
            <div  class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    <div class="col-md-1 col-sm-1 col-xs-1" ></div>
                    <div class="col-md-11 col-sm-11 col-xs-11">
                     <!-- začátek galerie -->
                        <div id="blueimp-gallery" class="blueimp-gallery">
                            <!-- The container for the modal slides -->
                            <div class="slides"></div>
                            <!-- Controls for the borderless lightbox -->
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close">×</a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                            <!-- The modal dialog, which will be used to wrap the lightbox content -->
                            <div class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"></h4>
                                        </div>
                                        <div class="modal-body next"></div>
                                        <div class="modal-footer">
                                            <button type="button" style="background-color: #2A3F54; color: #ffffff;" class="btn btn-default pull-left prev">
                                                <i class="glyphicon glyphicon-chevron-left"></i>
                                                Předchozí
                                            </button>
                                            <button type="button" style="background-color: #027db3; color:#ffffff;;" class="btn btn-defualt pull-right next">
                                                Další
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- konec galerie -->
                    </div>

                    <div class="clearfix" style="background-color: #ededed;"></div>
                </div>
            </div>

        </div>
        <br />
        <? include'inc/footer.php'; ?>
    </div>
    <!-- / content -->
<? include'inc/bottom.php';?>